function getDate() {

    var date = new Date(); // nowy obiekt z aktualną datą

    // Aktualny dzień miesiąca
    var partialDay = date.getDate();
    if (partialDay.toString().length === 1) {
        partialDay = '0' + partialDay;
    }

    // Aktualny miesiąc
    var partialMonth = date.getMonth() + 1;
    if (partialMonth.toString().length === 1) {
        partialMonth = '0' + partialMonth;
    }

    // Aktualny rok
    var partialYear = date.getFullYear();


    return partialDay + '.' + partialMonth + '.' + partialYear;
}