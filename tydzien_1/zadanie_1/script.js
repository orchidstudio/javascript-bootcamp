var quantity = 100; // zakupiona ilość produktu
var basePrice = 50; // bazowa cena produktu
var discount; // przyznany rabat
var finalPrice; // ostateczna cena produktu

// Wersja 1 - wykorzystana instrukcja if...else
discount = 0; // domyślny rabat 0%
if (quantity >= 5 && quantity <= 20) {
    discount = 5; // rabat 5%
} else if (quantity >= 21 && quantity <= 50) {
    discount = 10; // rabat 10%
} else if (quantity >= 51 && quantity <= 100) {
    discount = 15; // rabat 15%
} else if (quantity > 100) {
    discount = 20; // rabat 20%
}

finalPrice = basePrice * (100 - discount) / 100;
console.log('Wykorzystana instrukcja if...else: Podstawowa cena produktu to ' + basePrice + 'zł, po obniżce to ' + finalPrice + 'zł');

// Wersja 2 - wykorzystana instrukcja switch
discount = 0; // domyślny rabat 0%
switch (true) {
    case (quantity >= 5 && quantity <= 20):
        discount = 5; // rabat 5%
        break;
    case (quantity >= 21 && quantity <= 50):
        discount = 10; // rabat 10%
        break;
    case (quantity >= 51 && quantity <= 100):
        discount = 15; // rabat 15%
        break;
    case (quantity > 100):
        discount = 20; // rabat 20%
        break;
}

finalPrice = basePrice * (100 - discount) / 100;
console.log('Wykorzystana instrukcja switch: Podstawowa cena produktu to ' + basePrice + 'zł, po obniżce to ' + finalPrice + 'zł');