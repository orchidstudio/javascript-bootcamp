function sum(value) {

    if (arguments.length && value.length) {
        var sum = 0;
        for (i = 0; i < value.length; i++) {
            sum += value[i];
        }
        return sum;
    } else {
        console.log('Nie podano tablicy z wartościami!');
    }

}